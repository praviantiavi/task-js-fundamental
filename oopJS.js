class Team {
    
    //static property
    static isFromAmerica = true;
    
    constructor(name, color, rating) {
        this.name = name;
        this.color = color;
        this.rating = rating;
    }

    static isChampion(champ){
        if(champ > 0) {
            return "champion of ther world"
        }
        else {
            return "loser"
        }
    } 
    league() {
    console.log(`team ${this.name} have a ${this.color} color because they ${this.rating ? "win" : "lose"}`);

    
}
}

let Team1 = new Team("Patriots", "blue", false);
let Team2 = new Team("Saints", "peach", true);
let Team3 = new Team("Bengals", "orange", true);

console.log(Team.isFromAmerica); //static property

console.log(Team.isChampion(0)); //static method

Team1.league();

//child
class subTeam extends Team {
    constructor(name, color, subTeam) {
        super(name,color)
        this.subTeam = subTeam;
    }
    league() {
        super.league();
        console.log(`Team has another team called ${this.subTeam} `);
    }
    
}

let Team4 = new subTeam("raven","purple","barbatos");
Team4.league()
