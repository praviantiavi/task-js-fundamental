//nomer 1

const characters = ["klee", "albudi", "qiqi", "eula", "ganyu"]

//nomer 2

const biodata = {
    name: "klee",
    address: "mondo",
    vision: "pyro",
    favorite: {
        food: "toast",
        hobby: "fishing"
    }

}

//nomer 3

const [a, b, c, d, e] = characters
console.log(a, b, c, d, e)
console.log(b);

const {name, address, vision, favorite} = biodata
console.log(name, vision)
console.log(address, favorite)
const {food, hobby} = favorite
console.log(food);

//nomer 4
const birthday = 2 + " December"
console.log(`Ganyu birthday is ${birthday}`)

//nomer 5

var strong = 20;
var damage = (strong > 10) ? "very strong dps" : "support";
console.log(damage);

//nomer 6 
let weapons = [
    {name: "iron", rarity:3},
    {name: "dodoco", rarity:4},
    {name: "jade cutter", rarity: 5},
    {name: "hayafumi", rarity: 4.5},
    {name: "dull blade", rarity: 3.5}
    
    
]

//nomer 7
const weaponRarity = weapons.map(tes => tes.rarity);
const weaponName = weapons.map(tes => tes.name);
console.log(weaponName);
console.log(weaponRarity);


//nomer 8
const weaponRate = weapons.filter(tes => tes.rarity > 3)
console.log(weaponRate);

//nomer 9
const weaponRate1 = weapons.find(tes => tes.rarity > 3)
console.log(weaponRate1);

